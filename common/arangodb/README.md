### HELM

```
# The following will install the custom resources required by the operators.
helm install https://github.com/arangodb/kube-arangodb/releases/download/0.3.10/kube-arangodb-crd.tgz
# The following will install the operator for `ArangoDeployment` &
# `ArangoDeploymentReplication` resources.
helm install https://github.com/arangodb/kube-arangodb/releases/download/0.3.10/kube-arangodb.tgz
# To use `ArangoLocalStorage`, also run
helm install https://github.com/arangodb/kube-arangodb/releases/download/0.3.10/kube-arangodb-storage.tgz
```